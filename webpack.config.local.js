const path = require('path');
const webpack = require('webpack');
const WebpackNotifierPlugin = require("webpack-notifier");

const config = {
    entry: './src/main/js/index.js',
    cache: true,
    resolve: {
        extensions: ['.json', '.jsx', '.js'],
        modules: [
            path.resolve('./src/main/js'),
            path.resolve('./src/test/js'),
            "./node_modules"
        ]
    },
    mode: 'development',
    devtool: 'sourcemaps',
    devServer: {
        hot: true,
        contentBase: [path.join(__dirname, 'src/main/resources/static'),path.join(__dirname, 'src/main/resources/templates')],
        publicPath: '/',
        open: true,
        compress: true,
        port: 3000,
        historyApiFallback: true,
        proxy: {
            '/api/*': {
                //route ui dev-server which is on 8080 requests to the 9090 where backend server runs
                target: 'http://localhost:9090',
                secure: false
            },
            '/': {
                //serve the index.html from the server
                target: 'http://localhost:9090',
                secure: false
            }
        }
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new WebpackNotifierPlugin({ alwaysNotify: true, title: "OOCL RECOGNITION Build" })
    ],
    optimization: {
        minimize: false
    },
    output: {
        path: path.resolve(__dirname, 'src/main/resources/static'),
        filename: 'built/bundle.js',
        publicPath: '/'
    },
    module: {
        rules: [
            {
                test: path.join(__dirname, '.'),
                exclude: /(node_modules)/,
                loader: 'babel-loader',
                query: {
                    cacheDirectory: true,
                    presets: ['es2015', 'es2017', 'react', 'stage-0']
                }
            },
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader"]
            },
            {
                test: /\.scss$/,
                use: [
                    'style-loader?sourceMap=true',
                    'css-loader?sourceMap=true',
                    {
                        loader: 'sass-loader',
                        options: {
                            includePaths: ['node_modules', 'node_modules /@material/*'].map((d) => path.join(__dirname, d))
                        }
                    }
                ]
            },
            {
                test: /\.(jpe|jpg|png|woff|woff2|eot|ttf|svg)(\?.*$|$)/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[path][name].[ext]',
                        },
                    },
                ],
            },
        ]
    }
};

module.exports = config;