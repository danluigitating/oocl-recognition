package com.community.recognition.award;

import com.community.recognition.utils.AwardStatus;
import com.community.recognition.milestone.MilestoneService;
import com.community.recognition.notification.NotificationService;
import com.community.recognition.user.UserEntity;
import com.community.recognition.user.UserEntityBuilder;
import com.community.recognition.user.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AwardServiceTest {

    @Mock
    private AwardRepository awardRepository;
    @Mock
    private UserService userService;
    @Mock
    private MilestoneService milestoneService;
    @Mock
    private NotificationService notificationService;

    private AwardService awardService;


    @Before
    public void setUp() {
        awardService = new AwardService(awardRepository, userService, milestoneService, notificationService);
    }

    @Test
    public void getAll() {
        AwardEntity awardEntity = new AwardEntity(0L, 0L, 0L, false,
            "asdasd", "asdasd", "pending", "Jun17",
            "asdasdasd", 20, "asdasd", "asdasd","asdasd",
            "asdasdasd", "20190802");
        when(awardRepository.findAll()).thenReturn(Collections.singletonList(awardEntity));

        AwardEntity expectedAwardEntity = new AwardEntity(0L, 0L, 0L, false,
            "asdasd", "asdasd", "pending", "Jun17",
            "asdasdasd", 20, "asdasd", "asdasd","asdasd",
            "asdasdasd", "20190802");

        assertThat(awardService.getAll()).isEqualTo(Collections.singletonList(expectedAwardEntity));
    }

    @Test
    public void getAllByApprovalStatus() {
        AwardEntity awardEntity = new AwardEntityBuilder().initialData();
        when(awardRepository.findAllByApprovalStatus(AwardStatus.PENDING)).thenReturn(Collections.singletonList(awardEntity));
        UserEntity userEntity = new UserEntityBuilder().initialData();
        when(userService.getUserById(awardEntity.getAwardeeUserId())).thenReturn(Optional.of(userEntity));
        when(userService.getUserById(awardEntity.getRequestorUserId())).thenReturn(Optional.of(userEntity));

        PendingApprovalResponse response = new PendingApprovalResponseBuilder().initialData();
        response.setAwardeeName(userEntity.getFullName());
        response.setRequestorName(userEntity.getFullName());
        response.setSupervisor(true);
        response.setRequestDate("20190802");
        assertThat(awardService.getAllByApprovalStatus(AwardStatus.PENDING)).isEqualTo(Collections.singletonList(response));
    }

    @Test
    public void updateStatusToUpdatedOnUpdateAward() throws Exception{
        AwardEntity awardEntity = new AwardEntityBuilder().initialData();
        AwardResponse awardResponse = new AwardResponse(AwardStatus.UPDATED, "John Cruz Doe");
        when(awardRepository.existsById(1L)).thenReturn(true);

        UserEntity userEntity = new UserEntityBuilder().initialData();
        when(userService.getUserById(awardEntity.getAwardeeUserId())).thenReturn(Optional.of(userEntity));

        assertThat(awardService.updateAward(awardEntity, 1L, 2, "daluzde")).isEqualTo(awardResponse);
    }

    @Test
    public void updateStatusToApprovedOnUpdateAward() throws Exception{
        AwardEntity awardEntity = new AwardEntityBuilder().initialData();
        awardEntity.setApprovalStatus(AwardStatus.APPROVED);
        AwardResponse awardResponse = new AwardResponse(AwardStatus.APPROVED, "John Cruz Doe");
        when(awardRepository.existsById(1L)).thenReturn(true);

        UserEntity userEntity = new UserEntityBuilder().initialData();
        UserEntity approverEntity = new UserEntityBuilder().initialData();
        approverEntity.setFirstname("Dennis");
        approverEntity.setLastname("Daluz");
        approverEntity.setUsername("daluzde");
        when(userService.getUserById(awardEntity.getAwardeeUserId())).thenReturn(Optional.of(userEntity));
        when(userService.getUserByUsername(anyString())).thenReturn(userEntity);

        when(milestoneService.updateMilestone(awardEntity, 2)).thenReturn(true);
        doNothing().when(notificationService).broadcastToAll(any());

        when(awardRepository.save(any())).thenReturn(awardEntity);

        assertThat(awardService.updateAward(awardEntity, 1L, 2, "daluzde")).isEqualTo(awardResponse);
    }

    @Test
    public void getApprovedAwardById() {
        AwardEntity awardEntity = new AwardEntityBuilder().initialData();

        UserEntity userEntity = new UserEntityBuilder().initialData();
        when(userService.getUserById(awardEntity.getAwardeeUserId())).thenReturn(Optional.of(userEntity));
        when(userService.getUserById(awardEntity.getRequestorUserId())).thenReturn(Optional.of(userEntity));

        PendingApprovalResponse pendingApprovalResponse = new PendingApprovalResponseBuilder().initialData();
        pendingApprovalResponse.setAwardeeName(userEntity.getFullName());
        pendingApprovalResponse.setRequestorName(userEntity.getFullName());
        pendingApprovalResponse.setSupervisor(true);
        pendingApprovalResponse.setRequestDate("20190802");
        pendingApprovalResponse.setApprover("Dennis Daluz");
        when(awardRepository.findByIdAndApprovalStatus(1L, AwardStatus.APPROVED)).thenReturn(Optional.of(awardEntity));

        assertThat(awardService.getApprovedAwardById(1L, "Dennis Daluz")).isEqualTo(pendingApprovalResponse);
    }

    @Test
    public void saveAward() {
        AwardEntity awardEntity = new AwardEntityBuilder().initialData();
        when(awardRepository.save(any())).thenReturn(awardEntity);
        when(userService.getUserById(anyLong())).thenReturn(Optional.of(new UserEntityBuilder().initialData()));
        doNothing().when(notificationService).broadcastToAll(any());

        assertThat(awardService.saveAward(awardEntity)).isEqualTo(awardEntity);
    }
}