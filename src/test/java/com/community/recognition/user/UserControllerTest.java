package com.community.recognition.user;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
public class UserControllerTest {

    private MockMvc mockMvc;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Mock
    private UserService mockUserService;

    @Before
    public void setUp() {
        UserController userController = new UserController(mockUserService);
        mockMvc = MockMvcBuilders.standaloneSetup(userController).build();
    }

    @Test
    public void getAllUser() throws Exception {
        List<UserEntity> userEntities= new ArrayList<>();

        UserEntity userEntity = new UserEntity(0L,"dopey09","doofus","USER","Dobby",
                "The","Elf","Hogwarts","Dobby10@gmail.com","Harry");
        userEntities.add(userEntity);

        when(mockUserService.getAll()).thenReturn(userEntities);

        mockMvc.perform(get("/api/users/getAll")
                .accept(MediaType.parseMediaType(MediaType.APPLICATION_JSON_UTF8_VALUE)))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(userEntities)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0]").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[*]").isNotEmpty());
    }

    @Test
    public void getUser() throws  Exception{

        Optional<UserEntity> userEntity = Optional.of(new UserEntity(0L,"dopey09","doofus","USER","Dobby",
                "The","Elf","Hogwarts","Dobby10@gmail.com","Harry"));

        when(mockUserService.getUserById(0L)).thenReturn(userEntity);

        mockMvc.perform( MockMvcRequestBuilders
                .get("/api/users/getUser/{id}", 0)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(0));
    }

    @Test
    public void addUser () throws Exception{
        UserEntity userEntity =  new UserEntity(0L,"dopey09","doofus","USER","Dobby",
                "The","Elf","Hogwarts","Dobby10@gmail.com","Harry");

        when(mockUserService.addUser(userEntity)).thenReturn(userEntity);

        mockMvc.perform( MockMvcRequestBuilders
                .post("/api/users/addUser")
                .content(objectMapper.writeValueAsString(new UserEntity(0L,"dopey09","doofus","USER","Dobby",
                        "The","Elf","Hogwarts","Dobby10@gmail.com","Harry")))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists());
    }

    @Test
    public void updateUser () throws Exception{
        UserEntity origUserEntity = new UserEntity(0L,"dopey09","doofus","USER","Dobby",
                "The","Elf","Hogwarts","Dobby10@gmail.com","Harry");

        UserEntity updateUserEntity = new UserEntity(0L,"dopey09","doofus","USER","Kreacher",
                "The","Elf","Hogwarts","Dobby10@gmail.com","Harry");

        mockUserService.addUser(origUserEntity);
        when(mockUserService.updateUser(updateUserEntity,1L)).thenReturn(updateUserEntity);

        mockMvc.perform(MockMvcRequestBuilders
                .put("/api/users/updateUser/{id}", "1")
                .content(objectMapper.writeValueAsString(updateUserEntity))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(updateUserEntity)));

    }

    @Test
    public void deleteAward() throws Exception
    {
        UserEntity userEntity = new UserEntity(0L,"dopey09","doofus","USER","Dobby",
                "The","Elf","Hogwarts","Dobby10@gmail.com","Harry");
        mockUserService.addUser(userEntity);
        mockUserService.deleteUser(1L);

        mockMvc.perform( MockMvcRequestBuilders.delete("/api/users/deleteUser/{id}", 1) )
                .andExpect(status().isAccepted());
    }

}
