package com.community.recognition.notification;

import com.community.recognition.utils.AwardStatus;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.messaging.simp.SimpMessageSendingOperations;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class NotificationServiceTest {
    private final String destination = "/topic/all";

    private NotificationService notificationService;

    @Mock
    private SimpMessageSendingOperations template;

    @Before
    public void setUp() {
        notificationService = new NotificationService(template);
    }

    @Test
    public void broadcastToAll() {
        NotificationMessage message = new NotificationMessage(1L, AwardStatus.APPROVED, "Test", "Alien.jpeg", "Dennis Daluz");
        doNothing().when(template).convertAndSend(eq(destination), eq(message));

        notificationService.broadcastToAll(message);
        verify(template, times(1)).convertAndSend(eq(destination), eq(message));
    }
}