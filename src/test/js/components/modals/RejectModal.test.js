import {shallow} from 'enzyme'
import React from "react"
import {RejectModal} from "../../../../main/js/components/modals/RejectModal"
import {Modal} from "semantic-ui-react"

describe('Reject Modal test ', () => {
    let props, wrapper, wrapperInstance

    beforeEach(() => {
        props = {
            actions: {
                rejectAward: async() => jest.fn(),
                fetchAllPendingApproval: jest.fn()
            },
            closeAction: jest.fn()
        }
        wrapper = shallow(<RejectModal {...props} />)
        wrapperInstance = wrapper.instance()
    })

    it('renders the component', () => {

        expect(wrapper).toHaveComponent(Modal)
        expect(wrapper).toHaveComponent("TextArea")
        expect(wrapper.find("TextArea").props().placeholder).toEqual("Please provide details on why the request was rejected...")
        let buttons = wrapper.find("Button")
        expect(buttons.length).toEqual(2)
        expect(buttons.at(0).props().children).toEqual("OK")
        expect(buttons.at(1).props().children).toEqual("Cancel")
        const alertModal = wrapper.find('AlertModal')
        expect(alertModal.length).toBe(1)
    })

    it('should handle alert modal submit', () => {
        wrapperInstance.handleOnAlertModalSubmit()
        expect(wrapperInstance.state.showAlertModal).toBeFalsy()
    })

    it('should handle remarks change', () => {
        const event = {
            target: {
                value: 'remarks'
            }
        }
        expect(wrapperInstance.state.rejectionRemarks).toEqual('')
        wrapperInstance.handleRemarksChange(event)
        expect(wrapperInstance.state.rejectionRemarks).toEqual('remarks')
    })
})