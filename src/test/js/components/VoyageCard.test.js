import {shallow} from 'enzyme'
import React from "react"
import {Card, CardDescription, CardHeader, CardMeta, Image} from "semantic-ui-react"
import VoyageCard from "../../../main/js/components/VoyageCard"

describe('Voyage Card  ', () => {
    it('renders the component', () => {
        let data = {
            name : 'Almost',
            team: 'DPMCP',
            description: 'dummy Description',
            imageSrc: 'images/dummyImage.jpg'
        }
        const wrapper = shallow(<VoyageCard data={data}/>)
        expect(wrapper).toHaveComponent(Card)
        let image = wrapper.find(Image)
        expect(image.length).toEqual(1)
        expect(image.props().src).toEqual("images/dummyImage.jpg")

        let cardHeader = wrapper.find(CardHeader)
        expect(cardHeader.length).toEqual(1)
        expect(cardHeader.props().children).toEqual("Almost")

        let cardMeta = wrapper.find(CardMeta)
        expect(cardMeta.length).toEqual(1)
        expect(cardMeta.props().children).toEqual("DPMCP")

        let cardDescription = wrapper.find(CardDescription)
        expect(cardDescription.length).toEqual(1)
        expect(cardDescription.props().children).toEqual("dummy Description")
    })

})