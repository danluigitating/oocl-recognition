import {shallow} from 'enzyme'
import React from "react"
import {Menu} from "semantic-ui-react"
import {NavBar} from "../../../main/js/components/NavBar"

describe('NavBar ', () => {
    it('renders the component', () => {
        const wrapper = shallow(<NavBar/>)
        expect(wrapper).toHaveComponent(Menu)
    })

})