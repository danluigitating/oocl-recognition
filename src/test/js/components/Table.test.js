import {shallow, mount} from 'enzyme'
import React from "react"
import {Header, HeaderSubheader, Table as TableSemantic, TableBody, TableHeader} from "semantic-ui-react"
import {Table} from "../../../main/js/components/Table"
import {findManyComponents} from "../testHelpers"

describe('Table  ', () => {
    let table, props

    beforeEach(() => {
        props = {
            datas: [{
                id: 0,
                awardee: 'John Doe',
                requestor: 'Jonathan Doe',
                remarks: 'John Doe helped us do something something something something'
            }],
            columns: [{
                field: 'eggnog',
                label: 'Eggnog',
                renderer: jest.fn()
            }, {
                field: 'chippy',
                label: 'Chippy',
                renderer: jest.fn()
            }]
        }

        table = shallow(<Table {...props}/>)
    })

    it('should render table with header and columns', () => {
        const wrapper = shallow(<Table {...props}/>)
        expect(wrapper).toHaveComponent(TableSemantic)
        expect(wrapper).toHaveComponent(TableHeader)
        expect(wrapper).toHaveComponent(TableBody)
    })

    it('should render table with no items', () => {
        const newProps = {
            ...props,
            datas: [],
            columns: []
        }
        const wrapper = shallow(<Table {...newProps}/>)
        expect(wrapper).not.toHaveComponent(TableHeader)
        expect(wrapper).not.toHaveComponent(TableBody)
    })


    it('should render table with column but has no header', () => {
        const newProps = {...props, columns: []}
        const wrapper = shallow(<Table {...newProps}/>)
        expect(wrapper).toHaveComponent(TableBody)
        expect(wrapper).not.toHaveComponent(TableHeader)
    })


})
