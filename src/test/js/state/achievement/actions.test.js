import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'

import * as actions from '../../../../main/js/state/achievement/actions'
import * as types from '../../../../main/js/actionTypes'
import axios from "axios"

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)


describe('Achievement actions', () => {
    let store
    beforeEach(() => {
        store = mockStore({})
    })

    it('handles fetchAllAchievements', () => {
        const expectedActions = [
            {type: types.FETCH_ACHIEVEMENTS}
        ]
        const mockReturnValue = [{
            id : 1,
            name : 'John Cena',
        }]
        const expectedURL = '/api/achievements'
        jest.spyOn(axios, "get").mockReturnValueOnce(Promise.resolve(mockReturnValue))

        return store.dispatch(actions.fetchAllAchievements()).then(()=>{
            expect(axios.get).toHaveBeenCalledWith(expectedURL)
            expect(store.getActions()).toEqual(expectedActions)
            axios.get.mockReset()
        })

    })
})