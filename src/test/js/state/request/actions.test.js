import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'

import * as actions from '../../../../main/js/state/request/actions'
import * as types from '../../../../main/js/actionTypes'
import * as dateUtil from '../../../../main/js/util/DateUtil'
import axios from "axios"

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)


describe('App actions', () => {
    let store, deriveDateTodaySpy

    beforeEach(() => {
        store = mockStore({})
        deriveDateTodaySpy = jest.spyOn(dateUtil, 'deriveDateToday').mockReturnValueOnce('20190802')
    })

    it('handles submission of request', () => {
        Object.defineProperty(window.location, 'reload', {
            configurable: true,
        })
        const expectedURL = '/api/awards/saveAward'
        jest.spyOn(axios, "post").mockReturnValueOnce(Promise.resolve())

        return store.dispatch(actions.submitRequest({value: "dummyRequestEntity"})).then(() =>{
            expect(deriveDateTodaySpy).toHaveBeenCalled()
            expect(axios.post).toHaveBeenCalledWith(expectedURL,JSON.stringify({value: 'dummyRequestEntity', requestedDate: '20190802'}))
        })
    })
})