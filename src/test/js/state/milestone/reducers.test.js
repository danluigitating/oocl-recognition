import reducer from "../../../../main/js/state/milestone/reducers"
import {checkReducerFn, clone} from "../../testHelpers"
import * as types from "../../../../main/js/actionTypes"

describe('milestone reducers', () => {
    let checkReducer

    const initialState = {
        voyage: [],
        table: []
    }

    const initialStateClone = clone(initialState)

    beforeEach(()=>{
        checkReducer = checkReducerFn.bind(null, reducer, initialStateClone, initialState)
    })

    it('should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual(initialState)
    })

    it('should handle fetch all milestone', () => {
        const voyage = [{
            description: "1 Voyage",
            imageSrc: "/images/avatar/alien.jpeg",
            name: "Ziegfreid Morissey Flameño",
            team: "ISD"
        }]
        const table = [{
            id: 0,
            imageSrc: "/images/avatar/alien.jpeg",
            name: "Ziegfreid Morissey Flameño",
            stars: 28,
            team: "ISD"
        }]
        const action = {
            type: types.FETCH_ALL_MILESTONES,
            payload: {
                voyage: voyage,
                table: table
            }
        }
        const expectedState = clone(initialState)
        expectedState.voyage = voyage
        expectedState.table = table
        checkReducer(action, expectedState)
    })

})