import {shallow} from 'enzyme'
import React from "react"
import {HomePage} from "../../../main/js/pages/HomePage"
import {Grid} from "semantic-ui-react"
import {findManyComponents} from "../testHelpers";

describe('Homepage test', () => {
    it('renders the component', () => {
        const wrapper = shallow(<HomePage/>)
        expect(wrapper).toHaveComponent(Grid)

        let headers = findManyComponents(wrapper, "Header")
        const actualHeaders = ["For My Approval", "My Pending Request","Recently Awarded"]
        headers.forEach((header)=>{
            expect(header.props().as).toEqual("h1")
            expect(actualHeaders).toContain(header.props().children)
        })
    })
})