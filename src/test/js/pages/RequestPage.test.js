import {shallow} from 'enzyme'
import React from "react"
import {RequestPage} from "../../../main/js/pages/RequestPage"
import {Button} from "semantic-ui-react"
import SemanticDatepicker from "react-semantic-ui-datepickers"
import attributes from "../../../main/js/util/attributes"

describe('Request Page Test ', () => {
    let props, wrapper, wrapperIntance

    beforeEach(() => {
        props = {
            actions:{
                submitRequest: async() => jest.fn()
            },
            users :{
                usersDropDown : []
            }
        }
        wrapper = shallow(<RequestPage {...props}/>)
        wrapperIntance = wrapper.instance()
    })

    it('renders the component', () => {
        let header = wrapper.find("Header[as='h1']")
        expect(header.first().props().children).toEqual('New Request')

        let sectionHeaders = wrapper.find("Header[as='h3']")
        expect(sectionHeaders.length).toEqual(3)
        expect(sectionHeaders.at(0).props().children).toEqual("Awardee's Info")
        expect(sectionHeaders.at(1).props().children).toEqual("Requestor's Info")
        expect(sectionHeaders.at(2).props().children).toEqual("Award Info - Check the appropriate box next to the attribute(s) listed below")

        let datePicker = wrapper.find(SemanticDatepicker)
        expect(datePicker.length).toEqual(1)

        let awardeeEditorSection = wrapper.find('#awardee-details')
        let awardeeInputFields = awardeeEditorSection.find('Input')
        expect(awardeeInputFields.length).toEqual(1)
        expect(awardeeInputFields.at(0).props().name).toEqual('awardeeTeam')

        let requestorInputFields = wrapper.find("#requestor-details").find('Input')
        expect(requestorInputFields.length).toEqual(1)
        expect(requestorInputFields.at(0).props().name).toEqual('requestorTeam')

        let supervisorCheckbox = wrapper.find('#requestor-details').find('Checkbox')
        expect(supervisorCheckbox.length).toEqual(1)
        expect(supervisorCheckbox.props().name).toEqual('supervisor')

        let attributeGrid = wrapper.find('#attribute-list').find('Grid')
        let attributeCheckboxes = attributeGrid.find('CheckboxAttributes')
        const attributeList = attributes.map((attribute)=>(attribute.label))
        expect(attributeCheckboxes.length).toEqual(23)
        attributeCheckboxes.forEach((checkbox)=>{
            expect(attributeList).toContain(checkbox.props().label)
        })
        let othersField = attributeGrid.find('Input')
        expect(othersField.length).toEqual(1)

        let remarksField = wrapper.find('TextArea[name="remarks"]')
        expect(remarksField.length).toEqual(1)
        expect(remarksField.first().props().placeholder).toEqual("Please tell us how this experience has been positive for you...")

        let buttons = wrapper.find(Button)
        expect(buttons.length).toEqual(2)
        expect(buttons.at(0).props().children).toEqual('Submit')
        expect(buttons.at(1).props().children).toEqual('Reset')
    })

    it('should handle on modal submit', () => {
        wrapperIntance.handleOnModalSubmit()

        const state = wrapperIntance.state
        expect(state.showAlertModal).toBeFalsy()
        expect(state.requestorUserId).toEqual('')
        expect(state.requestorTeam).toEqual('')
        expect(state.requestorDepartment).toEqual('')
        expect(state.supervisor).toBeFalsy()
        expect(state.awardeeUserId).toEqual('')
        expect(state.awardeeTeam).toEqual('')
        expect(state.awardeeDepartment).toEqual('')
        expect(state.remarks).toEqual('')
        expect(state.otherAttribute).toEqual('')
        expect(JSON.stringify(state.attributes)).toEqual(JSON.stringify(attributes))
    })

    it('should handle date change', () => {
        const date = new Date('2019','06','24')
        wrapperIntance.handleOnDateChange(date)

        expect(wrapperIntance.state.requestDate).toEqual(date)
    })

    it('submits the form', ()=>{
        wrapperIntance.handleOnSubmit()

        let expectedForm = {
            requestorName: '',
            requestorTeam: '',
            requestorDepartment: '',
            supervisor: true,
            awardeeName: '',
            awardeeTeam: '',
            awardeeDepartment: '',
            remarks: '',
            attributes: '',
            requestDate:'',
            approvalStatus:'',
            requestorUserId:1,
            awardeeUserId:0
        }

        props.actions.submitRequest(JSON.stringify(expectedForm)).then(() => {
            expect(wrapperIntance.state.showAlertModal).toBeTruthy()
        })
    })

    it('should handle checkbox change for attributes', () => {
        const attributeCopy = JSON.parse(JSON.stringify(attributes))
        wrapper.setState({attributes: attributeCopy})
        wrapper.update()
        wrapperIntance = wrapper.instance()

        wrapperIntance.state.attributes.forEach(attribute => {
            expect(attribute.value).toBeFalsy()
        })

        wrapperIntance.handleCheckboxChange(1, null, {checked: true})
        wrapperIntance.handleCheckboxChange(10, null, {checked: true})

        wrapperIntance.state.attributes.forEach(attribute => {
            if(['Follow-Up', 'Skill Application'].includes(attribute.label)) {
                expect(attribute.value).toBeTruthy()
            }else{
                expect(attribute.value).toBeFalsy()
            }
        })
    })

    it('should handle checkbox change for supervisor', () => {
        const attributeCopy = JSON.parse(JSON.stringify(attributes))
        wrapper.setState({attributes: attributeCopy})
        wrapper.update()
        wrapperIntance = wrapper.instance()

        expect(wrapperIntance.state.supervisor).toBeFalsy()

        wrapperIntance.handleCheckboxChange(1, null, {checked: true, name: 'supervisor'})

        wrapperIntance.state.attributes.forEach(attribute => {
            expect(attribute.value).toBeFalsy()
        })
        expect(wrapperIntance.state.supervisor).toBeTruthy()
    })

    it('should handle input change', () => {
        wrapperIntance.handleInputFieldChange({target: {name: 'awardeeName', value: 'Jane Doe'}})
        expect(wrapperIntance.state.awardeeName).toEqual('Jane Doe')
    })

    it('should handle dropdown change', () => {
        wrapperIntance.dropDownOnChange('awardeeDepartment', {}, {value: 'OPERATIONS'})
        expect(wrapperIntance.state.awardeeDepartment).toEqual('OPERATIONS')
        wrapperIntance.dropDownOnChange('awardeeDepartment', {}, {value: 'DOCUMENTATION'})
        expect(wrapperIntance.state.awardeeDepartment).toEqual('DOCUMENTATION')
    })

    // it('submits the form without other attribute', ()=>{
    //     wrapper.setState({otherAttribute: ''})
    //     wrapper.instance().handleOnSubmit()
    //     let expectedForm = {
    //         requestorName: 'Test',
    //         requestorTeam: 'asdsadsad',
    //         requestorDepartment: 'asdsad',
    //         supervisor: true,
    //         awardeeName: '123213',
    //         awardeeTeam: 'qweqwewqe',
    //         awardeeDepartment: 'asdasdasdsad',
    //         remarks: '123123213',
    //         attributes: '',
    //         requestDate:'asdasdsad',
    //         approvalStatus:'PENDING',
    //         requestorUserId:1,
    //         awardeeUserId:0
    //     }
    //
    //     expect(props.actions.submitRequest).toHaveBeenCalledWith(JSON.stringify(expectedForm))
    // })
})