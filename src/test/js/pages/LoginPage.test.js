import {shallow} from 'enzyme'
import React from "react"
import {LoginPage} from "../../../main/js/pages/LoginPage"
import {Form} from "semantic-ui-react"

describe('Login Page', () => {
    let props, wrapper

    beforeEach(() => {
        props = {}
        wrapper = shallow(<LoginPage {...props}/>)
    })

    it('renders the component', () => {
        expect(wrapper).toHaveComponent(Form)
    })

})