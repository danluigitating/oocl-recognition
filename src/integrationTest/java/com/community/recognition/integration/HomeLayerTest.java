package com.community.recognition.integration;

import static org.fluentlenium.assertj.FluentLeniumAssertions.assertThat;

import org.fluentlenium.core.domain.FluentWebElement;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class HomeLayerTest extends BaseIntegrationTest {
    private final String[] ATTRIBUTES = {"Positive", "Follow-Up", "Creativity", "Respect others Point of View",
            "Honesty", "Dedication", "Thoroughness", "Empower & Develop others", "Sharing", "Accuracy",
            "Skill Application", "Shares Info & Encourage Discussion", "Flexible", "Timeliness", "Extra Effort",
            "Recognizes achievements & gives credit", "Caring", "Sensitivity", "Can Do Attitude", "Customer Appreciation Letter",
            "Quality", "Responsiveness", "Is a Team Member", "Other"};
    private final String[] APPROVAL_REQUEST_DETAILS = {"Awardee", "Team", "Cluster", "Requestor",
            "Team", "Cluster", "Awardee Supervisor", "Request Date", "Attributes", "Remarks"};

    @Before
    public void setup() {

    }

    @Test
    public void testRequestLayer() {
        login("daluzde", "Password1");
        routingNavigationTest();
    }

    public void routingNavigationTest() {
        goToPage("Request");

        WebDriver driver = this.getDriver();
        WebElement webElement = driver.findElement(By.xpath(".//div[@id='recog-sidebar']"));
        List<WebElement> sidebarList = webElement.findElements(By.xpath(".//a/i"));

        sidebarList.get(0).click();
        FluentWebElement page = $(".stretched.fourteen.wide.column").first();
        assertThat(page.$("h1").first().html()).isEqualTo("Completed Voyages");
        assertThat(page.$("h1").last().html()).isEqualTo("Container Count");

        FluentWebElement voyagesCard = page.$(".stretched.two.column.row.base-grid-row .stretched.column").first();
        assertThat(voyagesCard.$(".content").first().textContent()).contains("Ziegfreid Morissey Flame");

        FluentWebElement containerCount = page.$(".stretched.two.column.row.base-grid-row .column").first();
        assertThat(containerCount.$(".header").first().textContent()).contains("Ziegfreid Morissey Flame");

        sidebarList.get(1).click();
        FluentWebElement firstRow = page.$(".two.column.row").first();
        assertThat(firstRow.$(".ui.header").first().textContent()).isEqualTo("New Request");
        FluentWebElement datepicker = firstRow.$(".right.floated.four.wide.column").first();
        assertThat(datepicker.$("span").first().textContent()).isEqualTo("Request Date:");
        assertThat(datepicker.$("#semantic-date-picker").first()).isDisplayed();
        assertDetails(true, page, "#awardee-details");
        assertDetails(false, page, "#requestor-details");
        assertAttributes(page);
        assertRemarksAndSave(page);

        sidebarList.get(2).click();
        assertPendingRequestAndRequestInfo(page);
        assertApprovalDetails(page);
    }

    public void assertDetails(boolean isAwardee, FluentWebElement page, String id) {
        String header = isAwardee ? "Awardee's Info" : "Requestor's Info";
        FluentWebElement element = page.$(".two.column.row").last().$(id).first();

        if (!isAwardee) {
            assertThat(element.$(".content").first().textContent()).contains("Awardee's Supervisor:");
        }

        assertThat(element.$(".ui.header").first().textContent()).isEqualTo(header);
        assertThat(element.$(".content").first().textContent()).contains("Name:");
        assertThat(element.$(".content").first().textContent()).contains("Team:");
        assertThat(element.$(".content").first().textContent()).contains("Cluster:");
    }

    private void assertAttributes(FluentWebElement page) {
        FluentWebElement attributesRow = page.$(".one.column.row").first();
        assertThat(attributesRow.$(".ui.header").first().textContent()).isEqualTo("Award Info - Check the appropriate box next to the attribute(s) listed below");
        String attributes = attributesRow.$(".awardee-attributes").first().textContent();
        for (String attribute : ATTRIBUTES) {
            assertThat(attributes).contains(attribute);
        }
    }

    private void assertRemarksAndSave(FluentWebElement page) {
        FluentWebElement remarksAndSave = page.$(".equal.width.row").first();
        assertThat(remarksAndSave.$("textarea[name='remarks']").first().attribute("placeholder"))
                .isEqualTo("Please tell us how this experience has been positive for you...");
        assertThat(remarksAndSave.$(".column .ui.massive.button").first().textContent()).isEqualTo("Submit");
        assertThat(remarksAndSave.$(".column .ui.massive.button").last().textContent()).isEqualTo("Reset");
    }

    private void assertPendingRequestAndRequestInfo(FluentWebElement page) {
        assertThat(page.$(".two.column.row").first().$(".left.floated.column").first().textContent())
                .isEqualTo("Pending Request");
        assertThat(page.$(".two.column.row").first().$(".left.floated.column").first()
                .$("input[name='filterInput']").first().attribute("placeholder"))
                .isEqualTo("Filter by Name/Team...");
        assertThat(page.$(".two.column.row").first().$(".left.floated.column").last().textContent())
                .isEqualTo("Request Info");
    }

    private void assertApprovalDetails(FluentWebElement page) {
        FluentWebElement approvalDetails = page.$(".two.column.row").last();
        FluentWebElement toBeApproveColumn = approvalDetails.$(".column").first();
        assertThat(toBeApproveColumn.$(".ui.celled.collapsing.selectable.very.basic.table tr").first().textContent())
                .isEqualTo("AwardeeRemarksAction");

        FluentWebElement detailsColumn = approvalDetails.$(".column .content .ui.equal.width.grid").first();
        String content = detailsColumn.textContent();
        for (String x : APPROVAL_REQUEST_DETAILS) {
            assertThat(content).contains(x);
        }
    }
}
