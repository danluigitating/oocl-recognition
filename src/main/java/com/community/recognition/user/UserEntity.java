package com.community.recognition.user;

import java.io.IOException;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.StringUtils;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "user")
public class UserEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable=false)
    private Long id;

    @Column(name = "USERNAME", nullable = false, unique=true)
    private String username;

    @Column(name = "PASSWORD", nullable = false)
    private String password;

    @Column(name = "ROLE", nullable=false)
    private String role;

    @Column(name = "FIRSTNAME")
    private String firstname;

    @Column(name = "MIDDLENAME")
    private String middlename;

    @Column(name = "LASTNAME")
    private String lastname;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "TEAM")
    private String team;

    @Column(name = "CLUSTER")
    private String cluster;

    public String getFullName(){
        return Stream.of(this.firstname, this.middlename, this.lastname)
                    .filter(s-> !StringUtils.isEmpty(s)).collect(Collectors.joining(" "));
    }

    public String getAvatar() {
        try {
            //just add additional image format here
            String imageSrc = this.username + ".png";
            new ClassPathResource("/static/images/avatar/" + imageSrc).getFile();
            return imageSrc;
        } catch (IOException e) {
            return "alien.jpeg";
        }

    }
}
