package com.community.recognition.award;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface AwardRepository extends CrudRepository<AwardEntity, Long> {
    @Query(value = "select SUM(POINTS) from award where AWARDEE_USER_ID = ?1", nativeQuery = true)
    Optional<Integer> getUserPoints(long awardeeUserId);

    Iterable<AwardEntity> findAllByApprovalStatus(String approvalStatus);

    Optional<AwardEntity> findByIdAndApprovalStatus(Long id, String approvalStatus);
}
