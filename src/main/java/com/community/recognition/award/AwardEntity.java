package com.community.recognition.award;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Table(name = "AWARD")
@AllArgsConstructor
@NoArgsConstructor
public class AwardEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable=false)
    private Long id;

    @Column(name = "AWARDEE_USER_ID", nullable = false)
    private Long awardeeUserId;

    @Column(name = "REQUESTOR_USER_ID", nullable = false)
    private Long requestorUserId;

    @Column(name = "IS_SUPERVISOR")
    private boolean isSupervisor;

    @Column(name = "ATTRIBUTES")
    private String attributes;

    @Column(name = "REMARKS")
    private String remarks;

    @Column(name = "APPROVAL_STATUS")
    private String approvalStatus;

    @Column(name = "APPROVAL_DATE")
    private String approvalDate;

    @Column(name = "REJECTION_REMARKS")
    private String rejectionRemarks;

    @Column(name = "POINTS")
    private int points;

    @Column(name = "AWARDEE_TEAM", nullable = false)
    private String awardeeTeam;

    @Column(name = "AWARDEE_CLUSTER", nullable = false)
    private String awardeeCluster;

    @Column(name = "REQUESTOR_TEAM", nullable = false)
    private String requestorTeam;

    @Column(name = "REQUESTOR_CLUSTER", nullable = false)
    private String requestorCluster;

    @Column(name = "REQUESTED_DATE")
    private String requestedDate;

//    @ManyToOne(fetch = FetchType.EAGER)
//    @JoinColumn(name = "AWARDEE_USER_ID",  referencedColumnName = "ID")
//    private UserEntity awardeeUser;
//
//    @ManyToOne(fetch = FetchType.EAGER)
//    @JoinColumn(name = "REQUESTOR_USER_ID", referencedColumnName = "ID")
//    private UserEntity requestorUser;


}
