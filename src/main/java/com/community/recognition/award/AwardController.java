package com.community.recognition.award;

import com.community.recognition.utils.AwardStatus;
import java.util.Optional;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@AllArgsConstructor
@RestController
@RequestMapping("/api/awards")
public class AwardController {

    private AwardService awardService;

    @GetMapping(path = "/getAll", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Iterable<AwardEntity>> getAll() {
        return new ResponseEntity<>(awardService.getAll(), HttpStatus.OK);
    }

    @GetMapping(path = "/getAllPendingApprovals", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Iterable<PendingApprovalResponse>> getAllPendingApprovals() {
        return new ResponseEntity<>(awardService.getAllByApprovalStatus(AwardStatus.PENDING), HttpStatus.OK);
    }

    @GetMapping(path = {"/getApprovedAward/{id}", "/getApprovedAward/{id}/{approver}"}, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<PendingApprovalResponse> getApprovedAward(@PathVariable Long id,
           @PathVariable(required = false) Optional<String> approver) {
        return new ResponseEntity<>(awardService.getApprovedAwardById(id, approver.orElse("")), HttpStatus.OK);
    }

    @GetMapping(path = "/getAward/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<java.util.Optional<AwardEntity>> getAward(@PathVariable Long id) {
        return new ResponseEntity<>(awardService.getAward(id), HttpStatus.OK);
    }

    @PostMapping(path = "/saveAward", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<AwardEntity> saveAward(@RequestBody AwardEntity awardEntity) {
        return new ResponseEntity<>(awardService.saveAward(awardEntity), HttpStatus.CREATED);
    }

    @PostMapping(path = {"/updateAward/{id}", "/updateAward/{id}/{points}", "/updateAward/{id}/{points}/{approver}"},
        consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<AwardResponse> updateAward(@RequestBody AwardEntity awardEntity, @PathVariable Long id,
        @PathVariable(required = false) Optional<Integer> points,
        @PathVariable(required = false) Optional<String> approver) throws Exception {
        return new ResponseEntity<>(awardService.updateAward(awardEntity, id, points.orElse(2), approver.orElse("")), HttpStatus.OK);
    }


    @DeleteMapping(path = "/deleteAward/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<HttpStatus> deleteAward(@PathVariable Long id) throws Exception {
        awardService.deleteAward(id);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

}