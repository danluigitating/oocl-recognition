package com.community.recognition.milestone;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
@RequestMapping("/api/milestone")
public class MilestoneController {

    MilestoneService milestoneService;

    @GetMapping(path = "/getAll", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Iterable<MilestoneEntity>> getAll() {
        return new ResponseEntity<>(milestoneService.getAll(), HttpStatus.OK);
    }
}
