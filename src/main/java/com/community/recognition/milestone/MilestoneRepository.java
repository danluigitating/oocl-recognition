package com.community.recognition.milestone;

import org.springframework.data.repository.CrudRepository;

public interface MilestoneRepository extends CrudRepository<MilestoneEntity, Long> {

    MilestoneEntity findByUserId(Long userId);
}
