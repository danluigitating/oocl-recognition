package com.community.recognition.notification;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class NotificationService {

    @Getter
    private SimpMessageSendingOperations template;

    public void broadcastToAll(NotificationMessage notificationMessage) {
        template.convertAndSend("/topic/all", notificationMessage);
    }
}