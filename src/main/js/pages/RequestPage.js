import React, {Component} from 'react'
import {submitRequest} from "../state/request/actions"
import {connect} from "react-redux"
import {
    Button,
    Card,
    CardContent,
    Checkbox,
    Container,
    Dropdown,
    Form,
    Grid,
    GridColumn,
    GridRow,
    Header,
    Input,
    TextArea
} from "semantic-ui-react"
import attributes from "../util/attributes"
import {bindActionCreators} from "redux"
import CheckboxAttributes from "../components/CheckboxAttributes"
import SemanticDatepicker from "react-semantic-ui-datepickers"
import * as statics from "../util/statics"
import AlertModal from "../components/modals/AlertModal"
import UserDropDown from "../components/UserDropDown"
import {fetchAllUserDropDown} from "../state/user/actions"
import clusters from "../util/clusters"

export class RequestPage extends Component {

    constructor(props) {
        super(props)
        this.state = {
            requestorUserId: "",
            requestorTeam: "",
            requestorCluster: "",
            supervisor : false,
            awardeeUserId: "",
            awardeeTeam: "",
            awardeeCluster: "",
            remarks: "",
            attributes: attributes,
            requestDate: new Date(),
            otherAttribute: "",
            showAlertModal : false,
            alertTitle : 'Title',
            alertMessage:'Message'
        }
    }
    componentDidMount(){
        if(!this.props.users.usersDropDown){
           this.props.actions.fetchAllUserDropDown()
        }
    }

    handleInputFieldChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    handleCheckboxChange(index, event, data) {
        if (data.name === 'supervisor') {
            this.setState({
                supervisor: data.checked
            })
            return
        }

        //for attributes
        let attributesCopy = Object.assign([], this.state.attributes)
        attributesCopy[index].value = data.checked
        this.setState({
            attributes: [...attributesCopy]
        })
    }

    handleOnSubmit() {
        let attributes = []
        this.state.attributes.forEach((attribute) => attribute.value ? attributes.push(attribute.code) : null)
        let form = Object.assign({}, this.state)
        if(this.state.otherAttribute){
            attributes.push(this.state.otherAttribute)
        }
        form.attributes = attributes.toString()
        delete form.otherAttribute

        form = {...form,
            approvalStatus: statics.REQUEST_PENDING,
            requestorUserId: this.state.requestorUserId, //find userId in table
            awardeeUserId: this.state.awardeeUserId //find userId in table
        }

        this.props.actions.submitRequest(form).then(()=>{
            this.setState({
                alertTitle: 'Request Submitted',
                alertMessage: 'Request is successfully submitted for approval',
                showAlertModal : true
            })
        })
    }

    handleOnReset(){
        attributes.forEach((attribute) => {
            attribute.value = false
        })
        this.setState({
            requestorUserId: "",
            requestorTeam: "",
            requestorDepartment: "",
            supervisor : false,
            awardeeUserId: "",
            awardeeTeam: "",
            awardeeDepartment: "",
            remarks: "",
            attributes: attributes,
            requestDate: new Date(),
            otherAttribute: "",
            requestorCluster: "",
            awardeeCluster: ""
        })
    }

    handleOnDateChange(date) {
        this.setState({
            requestDate : date
        })
    }

    dropDownOnChange(name, event, {value}){
        this.setState({
            [name]: value
        })
    }

    handleOnModalSubmit() {
        this.handleOnReset()
        this.setState({
            showAlertModal: false
        })
    }

    getAwardeeEditor() {
        return (
            <div id='awardee-details'><Header as='h3'>Awardee's Info</Header>
                <Card fluid style={{height: 200, border:'2px solid black'}}>
                    <CardContent>
                        <div style={{display: 'flex', marginBottom: 15, alignItems: 'center'}}>
                            Name:
                            {this.props.users.usersDropDown &&
                            <UserDropDown style={{marginLeft: 10, flex: 1}} value={this.state.awardeeUserId}
                                          onChange={this.dropDownOnChange.bind(this, 'awardeeUserId')}
                                          options={this.props.users.usersDropDown}/>
                            }
                        </div>
                        <div style={{alignItems: 'center', display: 'flex', justifyContent: 'space-between'}}>
                            <div style={{paddingRight: 5, flexWrap: 'nowrap'}}> Team: <Input style={{marginLeft: 10}}
                                                                                             value={this.state.awardeeTeam}
                                                                                             name={"awardeeTeam"}
                                                                                             onChange={this.handleInputFieldChange.bind(this)}/>
                            </div>
                            <div>
                                Cluster:
                                <Dropdown className={'cluster-list'}
                                          search selection clearable
                                          options={clusters}
                                          onChange={this.dropDownOnChange.bind(this, 'awardeeCluster')}
                                          value={this.state.awardeeCluster}/>
                            </div>
                        </div>
                    </CardContent>
                </Card></div>
        )
    }

    getRequestorEditor() {
        return (<div id='requestor-details'>
            <Header as='h3'>Requestor's Info</Header>
            <Card fluid style={{height: 200, border: '2px solid black'}}>
                <CardContent>
                    <div style={{display: 'flex', marginBottom: 15, alignItems: 'center'}}>
                        Name:
                        {this.props.users.usersDropDown &&
                        <UserDropDown style={{marginLeft: 10, flex: 1}} value={this.state.requestorUserId}
                                      onChange={this.dropDownOnChange.bind(this, 'requestorUserId')}
                                      options={this.props.users.usersDropDown}/>
                        }
                    </div>
                    <div style={{alignItems: 'center', display: 'flex', justifyContent: 'space-between'}}>
                        <div style={{paddingRight: 5, flexWrap: 'nowrap'}}>
                            Team: <Input style={{marginLeft: 10}} value={this.state.requestorTeam}
                                         name={"requestorTeam"}
                                         onChange={this.handleInputFieldChange.bind(this)}/>
                        </div>
                        <div>
                            Cluster:
                            <Dropdown className={'cluster-list'}
                                      search selection clearable
                                      options={clusters}
                                      onChange={this.dropDownOnChange.bind(this, 'requestorCluster')}
                                      value={this.state.requestorCluster}/>
                        </div>
                    </div>
                    <div style={{display: 'flex', marginTop: 15}}>
                        Awardee's Supervisor: <Checkbox style={{marginLeft: 8}} name={"supervisor"} checked={this.state.supervisor} onChange={this.handleCheckboxChange.bind(this, null)}/>
                    </div>
                </CardContent>
            </Card>
        </div>)
    }

    getAttributeList() {
        return (<div id='attribute-list'>
            <Header as='h3'>Award Info - Check the appropriate box next to the attribute(s) listed below</Header>
            <Card fluid style={{border:'2px solid black'}}>
                <CardContent>
                    <div className='awardee-attributes'>
                        <Grid columns='equal'>
                            {
                                this.state.attributes.map((attribute, index) => (
                                    <CheckboxAttributes key={index}
                                                        width={((index + 1) % 4 === 0) ? 10 : 2}
                                                        label={attribute.label}
                                                        checkboxChanged={()=>this.handleCheckboxChange.bind(this, index)}
                                                        name={attribute.code}
                                                        checked={attribute.value}
                                    />
                                ))
                            }
                            <GridColumn width={10}>
                                Other: <Input name='otherAttribute' onChange={this.handleInputFieldChange.bind(this)}
                                              value={this.state.otherAttribute} style={{width: 350}}
                                              placeholder='Other Attributes'/>
                            </GridColumn>
                        </Grid>
                    </div>
                </CardContent>
            </Card>
        </div>)
    }

    render() {
        return (
            <Container fluid>

                <Grid style={{padding: 25}}>
                    <AlertModal open={this.state.showAlertModal}
                                onConfirm={this.handleOnModalSubmit.bind(this)}
                                message={this.state.alertMessage}
                                title={this.state.alertTitle}/>
                    <GridRow columns={2}>
                        <GridColumn width={5} floated='left'>
                            <Header as='h1'>New Request</Header>
                        </GridColumn>
                        <GridColumn width={4} floated='right'>
                            <span style={{margin:'0px 10px'}}>
                                Request Date:
                            </span>
                            <SemanticDatepicker
                                id="semantic-date-picker"
                                format={"MM/DD/YYYY"}
                                selected={this.state.requestDate}
                                onDateChange={this.handleOnDateChange.bind(this)}
                                maxDate={new Date()}
                            />
                        </GridColumn>
                    </GridRow>
                    <GridRow columns={2}>
                        <GridColumn>
                            {this.getAwardeeEditor()}
                        </GridColumn>
                        <GridColumn>
                            {this.getRequestorEditor()}
                        </GridColumn>
                    </GridRow>

                    <GridRow columns={1}>
                        <GridColumn>
                            {this.getAttributeList()}
                        </GridColumn>
                    </GridRow>
                    <GridRow columns='equal'>
                        <GridColumn width={12}>
                            <Form>
                                <TextArea
                                    style={{minHeight: 100, resize: 'none'}}
                                    placeholder='Please tell us how this experience has been positive for you...'
                                    value={this.state.remarks}
                                    name={"remarks"}
                                    onChange={this.handleInputFieldChange.bind(this)}
                                />
                            </Form>
                        </GridColumn>
                        <GridColumn style={{textAlign: 'center', margin: 'auto'}}>
                            <Button size='massive' style={{backgroundColor: '#EB5757', color: 'white'}} onClick={this.handleOnSubmit.bind(this)}>Submit</Button>
                            <Button size='massive' style={{backgroundColor: '#BDBDBD', color: 'white'}} onClick={this.handleOnReset.bind(this)}>Reset</Button>
                        </GridColumn>
                    </GridRow>
                </Grid>

            </Container>
        )
    }
}

function mapStateToProps(state) {
    return {
        requests: state.request.requests,
        users : state.users
    }
}

function mapDispatchToProps(dispatch) {
    return {actions: bindActionCreators({submitRequest, fetchAllUserDropDown}, dispatch)}
}

export default connect(mapStateToProps, mapDispatchToProps)(RequestPage)