import React, {Component} from 'react'
import {
    fetchAllPendingApproval,
    submitApproval
} from "../state/pendingApproval/actions"
import {connect} from "react-redux"
import {bindActionCreators} from "redux"
import {Button, Card, CardContent, Grid, GridColumn, GridRow, Header, Input} from "semantic-ui-react"
import Table from "../components/Table"
import RejectModal from "../components/modals/RejectModal"
import RequestInfoCard from "../components/RequestInfoCard"
import AlertModal from "../components/modals/AlertModal"
import * as statics from "../util/statics"

export class PendingApprovalPage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            filterInput: '',
            requestInfo: {},
            awardId: '',
            showAlertModal: false,
            showRejectModal: false,
            columns: [{
                field: 'awardee',
                label: 'Awardee',
                renderer: (data) => (
                    <Header as='h4' image>
                        <Header.Content>
                            {data.awardeeName}
                            <Header.Subheader>Requestor : {data.requestorName}</Header.Subheader>
                        </Header.Content>
                    </Header>
                )
            }, {
                field: 'remarks',
                label: 'Remarks'
            }, {
                field: 'action',
                label: 'Action',
                renderer: (data) => (
                    <div style={{display: 'grid', width: 150, margin:'auto'}}>
                        <Button id='approve-btn' size='tiny' style={{
                            backgroundColor: '#EB5757',
                            color: 'white',
                            margin: 2.5,
                            fontSize: 12,
                            fontWeight: 900
                        }}
                                onClick={this.handleOnApprove.bind(this)}>Approve</Button>
                        <Button id='reject-btn' size='tiny'
                                style={{backgroundColor: '#BDBDBD', color: 'white', margin: 2.5}}
                                onClick={this.handleOnReject.bind(this, data)}>Reject </Button>
                    </div>
                )
            }]
        }
    }

    componentDidMount() {
        this.props.actions.fetchAllPendingApproval()
    }

    handleOnApprove() {
        this.setState({
            showAlertModal: true
        })
    }

    handleOnReject(data) {
        this.setState({
            showRejectModal: true,
            awardId: data.id})
    }

    handleCloseModal = () => {
        this.setState({
          requestInfo: {},
          showRejectModal: false
        })
    }

    onRowClick(data) {
        this.setState({
            requestInfo: data
        })
    }

    handleInputFieldChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    handleOnAlertModalSubmit(clazz, object) {
        const points = object.children[0].props.content.split(' ')[0]
        const username = this.props.userProfile && this.props.userProfile.username
        this.props.actions.submitApproval(this.state.requestInfo, points, username)
        this.setState({
            showAlertModal: false,
            requestInfo: {}
        })
    }

    handleOnAlertModalClose() {
        this.setState({
            showAlertModal: false
        })
    }

    getFilteredApprovals() {
        if(!Array.isArray(this.props.pendingApprovals) || this.props.pendingApprovals.length === 0) {
            return
        }

        return this.props.pendingApprovals.filter((pendingApproval) => {
            let {awardeeName, requestorName, awardeeTeam, requestorTeam} = pendingApproval,
                filterInput = this.state.filterInput.toLowerCase()

            if (awardeeName.toLowerCase().includes(filterInput)) {
                return true
            }

            if (requestorName.toLowerCase().includes(filterInput)) {
                return true
            }

            if (awardeeTeam.toLowerCase().includes(filterInput)) {
                return true
            }

            if (requestorTeam.toLowerCase().includes(filterInput)) {
                return true
            }

        });
    }

    render() {
        let filteredApprovals = this.getFilteredApprovals()
        return (
            <div style={{display: 'flex'}}>
                <AlertModal open={this.state.showAlertModal}
                                onClose={this.handleOnAlertModalClose.bind(this)}
                                onConfirm={this.handleOnAlertModalSubmit.bind(this)}
                                pageType={statics.PENDING_APPROVAL_PAGE}
                                message={'Are you sure you want to approve?'}
                                title={'Approve Award'}/>
                <RejectModal open={this.state.showRejectModal} closeAction={this.handleCloseModal.bind(this)}
                             awardId={this.state.awardId}/>
                <Grid columns='equal' style={{display: 'flex', width: '100%', padding: 25}}>
                    <GridRow columns={2} style={{display: 'flex', alignContent: 'flex-end', alignItems: 'baseline'}}>
                        <GridColumn floated='left'>
                            <Header as='h1' style={{display: 'inline', paddingRight: 15}}>Pending Request</Header>
                            <Input name={'filterInput'} icon='filter' iconPosition='left'
                                   style={{height: 37, width: '65%', float: 'right'}}
                                   type="text"
                                   placeholder="Filter by Name/Team..."
                                   onChange={this.handleInputFieldChange.bind(this)}/>
                        </GridColumn>
                        <GridColumn floated='left'>
                            <Header as='h1'>Request Info</Header>
                        </GridColumn>

                    </GridRow>
                    <GridRow columns={2}>

                        <GridColumn>
                            <Card fluid>
                                <CardContent style={{display: 'flex', height: '71vh', overflow: 'auto'}}>
                                    {filteredApprovals ?
                                    <Table datas={filteredApprovals} columns={this.state.columns}
                                           onRowClick={this.onRowClick.bind(this)}/> :
                                        <div style={{margin:'auto'}}> There are no pending requests </div>
                                    }
                                </CardContent>
                            </Card>
                        </GridColumn>

                        <GridColumn>
                            <RequestInfoCard data={this.state.requestInfo}/>
                        </GridColumn>
                    </GridRow>
                </Grid>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        pendingApprovals: state.pendingApproval.pendingApprovals,
        userProfile: state.app.profile
    }
}

function mapDispatchToProps(dispatch) {
    return {actions: bindActionCreators({fetchAllPendingApproval,  submitApproval}, dispatch)}
}

export default connect(mapStateToProps, mapDispatchToProps)(PendingApprovalPage)