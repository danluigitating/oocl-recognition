import * as types from '../../actionTypes'
import UrlUtil from "../../util/UrlUtil"
import axios from 'axios'

const url = new UrlUtil('/api/users')

export const initializeApp = () => {
    return async (dispatch) => {
        dispatch({type: types.APP_INITIALIZED})
    }
}

export const getProfile = () => {
    return async (dispatch) => {
        console.log('DISPATCHING PROFILE')
        dispatch({type: types.FETCH_USER_PROFILE_REQUEST})
        const response = await axios.get(url.makeUrl(['profile']))
        dispatch({type: types.FETCH_USER_PROFILE_SUCCESS, payload: response.data})
    }
}

export const logError = () => {
    return (dispatch, getState) => {
        if (getState().app.hasError) {
              // already in an error state, dont re-dispatch and cause an infinite loop
              return
        }
        dispatch({type: types.HAS_ERROR})
    }
}

export const clearError = () => {
    return (dispatch) => {
        dispatch({type: types.CLEAR_ERROR})
    }
}