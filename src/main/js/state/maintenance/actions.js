import axios from 'axios'
import * as types from '../../actionTypes'


const url = '/api/user'

export const fetchAllUser = () => {
    return async (dispatch) => {
        const response = await axios.get(url)
        dispatch({type: types.FETCH_USER, payload: response.data})
    }
}