import * as types from '../../actionTypes'

const initialState = {
    userProfile : {}
}

export default function reducer(state = initialState, action) {
    switch (action.type) {

        case types.SAVE_USER: {
            return {
                ...state,
                userProfile: action.payload
            }
        }
        default:
            return state
    }
}