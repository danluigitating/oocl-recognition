import * as types from '../../actionTypes'

const initialState = {
    pendingApprovals: [],
    rejectModalOpen: false,
    approvedAward: null
}

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case types.FETCH_APPROVED_AWARD_REQUEST: {
            return {
                ...state,
                approvedAward: null
            }
        }
        case types.FETCH_APPROVED_AWARD_SUCCESS: {
            return {
                ...state,
                approvedAward: action.payload
            }
        }
        case types.FETCH_PENDING_APPROVAL: {
            return {
                ...state,
                pendingApprovals: action.payload
            }
        }
        // case types.CLOSE_REJECT_MODAL: {
        //     return {...state, rejectModalOpen: false}
        // }
        // case types.OPEN_REJECT_MODAL: {
        //     return {...state, rejectModalOpen: true}
        // }
        default:
            return state
    }
}