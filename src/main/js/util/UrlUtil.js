export default class UrlUtil {

  constructor(prefix) {
    if (prefix.slice(-1) === '/')
      throw new Error("path prefix must not have a trailing slash")

    this.prefix = prefix
  }

  encodeAppData(data) {
    if (!data) {
      console.log('bad data in encodeAppData')
      return ''
    }
    if (!data.includes('/')) {
      return encodeURIComponent(data)
    }
    return encodeURIComponent(data.split('').map(ea => ea === '/' ? '~!~' : ea).join(''))
  }

  static decodeAppData(data) {
    const result = decodeURIComponent(data)
    if (!result.includes('~!~')) {
      return result
    }
    return result.replace(/~!~/g,'/')
  }

  makeUrl(params) {
    let encodedPrefix = encodeURI(this.prefix);
    if (!params || !params.length) {
      return encodedPrefix
    }
    // We used to double encode our app data to handle cases where our data
    // contained a forward slash.  Latest spring boot has more strict firewall
    // and is not happy with an encoded % (or %20)
    // (first encoding changes / to %2F, second encoding changes to %202F).
    // I'm sure spring boot has good reasons to reject such urls.  It is
    // possible to tell tomcat to org.apache.tomcat.util.buf.UDecoder.ALLOW_ENCODED_SLASH
    // and tell spring to not decode encoded slashes.  However if we do that then the
    // app fails when trying to load a font name with an encoded slash:
    // SEVERE http://localhost:43413/fonts/Mark%20Simonson%20-%20Proxima%20Nova%20Bold.otf - Failed to load resource
    // As such latest approach is to replace forward slash in our data with a rare token (~!~)
    // which server side will detect and convert back to forward slash
    return encodedPrefix + '/' + params.map(ea => this.encodeAppData(ea)).join('/')
  }

}