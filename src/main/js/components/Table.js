import React, {Component} from 'react'
import {Table as TableSemantic} from "semantic-ui-react"

export class Table extends Component {
    constructor(props) {
        super(props)
    }

    onRowClick(data) {
        if (this.props.onRowClick) {
            this.props.onRowClick(data)
        }
    }

    createRows(data, columns) {
        return (
            <TableSemantic.Row key={data.id} onClick={() => this.onRowClick(data)}>
                {columns.map((column) => (
                    <TableSemantic.Cell
                        style={{verticalAlign: 'middle', paddingLeft: column.field === 'action' ? 0 : 20}}
                        key={column.field}>
                        {column.renderer ? column.renderer(data) : data[column.field]}
                    </TableSemantic.Cell>
                ))}
            </TableSemantic.Row>
        )
    }

    render() {
        let {columns, datas} = this.props


        return (
            datas.length &&
            <TableSemantic basic='very' celled collapsing style={{border: '2px solid black', width: '100%'}}
                           selectable>
                {
                    columns.length &&
                    <TableSemantic.Header>
                        <TableSemantic.Row>
                            {columns.map((column) => <TableSemantic.HeaderCell
                                style={{padding: 10, textAlign: 'center'}}
                                key={column.field}>{column.label}</TableSemantic.HeaderCell>)}
                        </TableSemantic.Row>
                    </TableSemantic.Header>
                }
                <TableSemantic.Body>
                    {datas.map((data) => (this.createRows(data, columns)))}
                </TableSemantic.Body>
            </TableSemantic>
        )


    }
}

export default Table