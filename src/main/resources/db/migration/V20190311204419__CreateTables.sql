CREATE TABLE award
(
  ID             INT(11)                NOT NULL AUTO_INCREMENT,
  REQUESTOR      varchar(50) default '' NOT NULL,
  REQUESTOR_TEAM varchar(50) default '' NOT NULL,
  IS_SUPERVISOR  varchar(50) default '' NOT NULL,
  AWARDEE        varchar(50) default '' NOT NULL,
  AWARDEE_TEAM   varchar(50) default '' NOT NULL,
  ATTRIBUTES     varchar(50) default '' NOT NULL,
  REMARKS        varchar(50) default '' NOT NULL,
  CREATED_DATE   DATE,
  PRIMARY KEY (ID)
);


CREATE TABLE user

(
  ID       INT(11)                NOT NULL AUTO_INCREMENT,
  USERNAME varchar(16) default '' NOT NULL,
  PASSWORD varchar(200) default '' NOT NULL,
  ROLE     varchar(50) default '' NOT NULL,
  PRIMARY KEY (ID)
);
