# Recognition System

# Heroku Deployment 
https://opi-recognition.herokuapp.com


Push a tag to trigger deployment in heroku.

Please check the tag name format and its incremental value e.g "v1.0.1 -> v1.0.2"
 

# Trello Board
https://trello.com/b/6lASHKXJ/opi-recognition

# Design Style
We are using this react semantic library - https://react.semantic-ui.com/

Product design page - https://www.figma.com/file/D93GSR8lQy3wIqBFRNVQPADd/Prototype?node-id=0%3A1


# Setup Project

````
1.Import in Intellij , then click the `build.gradle`, then leave the default settings.
2. After building the gradle project , run `npm install` . This will automatically install the frontend 
dependencies and setup the the `pre-push` git hooks.
3. Install lombok plugin in Intellij
4. Enable Annotation Processors in Intellij for Lombok 
CTRL + SHIFT + A  - Annotation Processors  
````
***INSTALL CHROMEDRIVER FOR INTEGRATION TEST TO WORK***

Documentation for ChromeDriver with installation steps for Linux and Windows- https://github.com/SeleniumHQ/selenium/wiki/ChromeDriver

***Linux***
````
sudo apt-get install chromium-chromedriver
````

***Windows***

 Install chocolatey first.  
 ````
 https://chocolatey.org/install
 ````
 
 Run your cmd in administrator mode then run this command

````
@"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"
````
Make sure to have a `chrome` browser installed in your machine.

Then run this command after installation of chocolatey ``choco install chromedriver``

After installing the chromedriver , restart your intellij

# Database Setup
**Linux SETUP** 

get mariadb docker image
````$xslt
docker pull mariadb
````

run script

````$xslt
//set -x //only include when creating a shell script in local
docker run --name mariadb -p 127.0.0.1:3306:3306 -e MYSQL_ROOT_PASSWORD=root -d mariadb:latest
````
To access terminal in docker:
````
docker exec -it <container_name> mysqldata_db_1 bash -c "mysql -uroot -proot"

//docker exec -it mariadb bash -c "mysql -uroot -proot"
````

then proceed to **Setup DB with Flyway** procedure



**Windows Setup**

1. Install MariaDB in you local PC

        https://mariadb.org/download/

or use ```choco install mariadb```
# Setup DB with Flyway
Create Database `recognition`

In you `build.gradle` this is the configuration
```$xslt
flyway {
    url = 'jdbc:mysql://localhost:3306'
    user = 'root'
    password = 'root'
    schemas = ['recognition']
}
```


You can just change it if you have different configuration E.g user and pass, 

Flyway will run every start of your spring. Please see the difference between `Version` and `Repeatable`

https://flywaydb.org/

# Doing DB Changes

Follow fly way rule. 

**IMPORTANT**

```Use small letters for table name , this includes your script```

# Linux Setup
for husky `pre-push` command, 
````$xslt
 "pre-push": "jest && ./gradlew test && ./gradlew integrationTest"
`````
Do not push the package json whenever editing this husky command. It will broke the Windows `pre-push` hook

# Running Integration Test

make sure to build the UI first. `npm run build`
then synchronize CTRL + ALT + Y


# Swagger UI

To use swagger UI , just run your local spring and access this address.
You can also insert data in Database easily by just using this UI. 

```
http://localhost:9090/swagger-ui.html
```

# Credentials for Sample Users

open ```creds.txt``` inside root folder

# Docker
we have our own docker image . See Dockerfile in the root folder

To build the image 
            
    docker build -t recognition:latest .
    docker run -it -v /home/audizz/Git/oocl-recognition:/home/gradle recognition /bin/bash
    
# Gitlab Pipeline Environment

     System:
        OS: Linux 4.19 Ubuntu 18.04.2 LTS (Bionic Beaver)
        CPU: (1) x64 Intel(R) Xeon(R) CPU @ 2.30GHz
        Memory: 152.83 MB / 3.61 GB
        Container: Yes
        Shell: 4.4.19 - /bin/bash
      Binaries:
        Node: 8.16.1 - /usr/bin/node
        Yarn: 1.17.3 - /usr/bin/yarn
        npm: 6.4.1 - /usr/bin/npm
      Utilities:
        Make: 4.1 - /usr/bin/make
        GCC: 7.4.0 - /usr/bin/gcc
        Git: 2.17.1 - /usr/bin/git
      Languages:
        Bash: 4.4.19 - /bin/bash
        Java: 1.8.0_212 - /opt/java/openjdk/bin/javac
        Perl: 5.26.1 - /usr/bin/perl
        Python: 2.7.15+ - /usr/bin/python
        Ruby: 2.5.1 - /usr/bin/rub